var settingClassList = [];

/**
 * Sorts the list of products
 */
function sortProducts(){

    //TODO: Add implementation
}

/**
 * Sorts the list of products
 */
function filterProducts(){

    //TODO: Add implementation
}

/**
 * Setting button click callback
 * @param e
 */
function onSettingClick(e){

    var body = document.getElementsByTagName("body")[0],
        currentTarget = e.currentTarget,
        settingOptions = document.getElementsByClassName("setting-option");

    currentTarget.classList.toggle("selected");

    for(var i = 0; i < settingClassList.length; i++){
        body.classList.remove(settingClassList[i]);
        if(currentTarget.getAttribute("id") == "clearSettings"){
            settingOptions[i].classList.remove("selected");
        }

    }

    body.classList.add(currentTarget.getAttribute("data-setting-class"));
}

window.addEventListener('load', function(){


    //Add event listeners
    var settings = document.getElementsByClassName("setting-option");

    for(var i = 0; i < settings.length; i++){
        settings[i].addEventListener("click", onSettingClick);
        var set = settings[i].getAttribute("data-setting-class");
        settingClassList.push(set);
    }

});
